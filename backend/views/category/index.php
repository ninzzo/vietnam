<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Category;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a(Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Category',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 




    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            //'user_id',
            //'description',            
            [
                'attribute' => 'image',
                'label'     => 'Иконка',
                'format'    => 'html',
                'value' => function ($model) {
                    return Html::img($model->thumbnail_base_url.'/'.$model->thumbnail_path,
                    ['width' => '16px']);
                },
               
            ],

            [
                'attribute' => 'category_id',
                'label'     => 'Основная категория',
                'value' => function ($model) {
                   return Category::main_category()[$model->category_id];
                },
                'filter' => Category::main_category(),
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
