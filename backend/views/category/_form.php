<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use trntv\filekit\widget\Upload;
use trntv\filekit\behaviors\UploadBehavior;
use common\models\Category;
/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    

    <?php
    $items = Category::main_category();
    $params = [
        'prompt' => 'Выберите категорию...'
    ];
    echo $form->field($model, 'category_id')->dropDownList($items,$params);
    ?>
    

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>  

    <?php echo $form->field($model, 'thumbnail')->widget(
    Upload::class,
    [
        'url' => ['/file/storage/upload'],
        'maxFileSize' => 5000000, // 5 MiB
    ]);
?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
