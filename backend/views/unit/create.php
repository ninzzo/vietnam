<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Unit */

$this->title = 'Добавить обьект';
$this->params['breadcrumbs'][] = ['label' => 'Обьекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
