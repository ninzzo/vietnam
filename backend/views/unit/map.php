 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>

 <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script> 
 <style type="text/css">
   	#mapid { height: 100vh; }
   </style>

<div id="mapid"></div>
<input type="text" id="address">
<input type="text" id="lat">
<input type="text" id="lon">
<script>


var mymap = L.map('mapid').setView([10.933211, 108.287184], 12);
//var mymap = L.map('mapid').setView([12.2450700, 109.1943200], 12);
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,		
		id: 'mapbox.streets'
	}).addTo(mymap);

	

  var marker = new L.marker([10.933211, 108.287184], {draggable: true}).addTo(mymap);
marker.on("dragend", function(e){
var lat = marker.getLatLng().lat.toFixed(8);
var lon = marker.getLatLng().lng.toFixed(8);
   $.get('https://nominatim.openstreetmap.org/reverse.php?format=json&lat='+lat+'&lon='+lon, function(data){
   	document.getElementById('address').value = data.display_name;
    });
document.getElementById('lat').value = lat;
document.getElementById('lon').value = lon;


});



</script>