<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use trntv\filekit\widget\Upload;
use trntv\filekit\behaviors\UploadBehavior;
use common\models\Category;
/* @var $this yii\web\View */
/* @var $model app\models\Unit */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="unit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название') ?>

    <?php echo $form->field($model, 'price')->textInput(['maxlength' => true])->label('Цена(VND). Указываем минимальную цену ОТ.') ?>

    <?php echo $form->field($model, 'fb')->textInput(['maxlength' => true])->label('facebook') ?>
    <?php echo $form->field($model, 'vk')->textInput(['maxlength' => true])->label('vk') ?>
    <?php echo $form->field($model, 'telegram')->textInput(['maxlength' => true])->label('telegram') ?>

    <?php 
    $district = [
      '1'=>'Муйне',
      '2'=>'Нячанг',
    ];
    echo $form->field($model, 'district')->dropDownList($district)->label('Район');
     ?>

    <?php 

   
    $result = [];
    $items = Category::main_category();
    foreach($items as $v=>$value){

      $asd = [];
      $category = Category::find()->where(['category_id'=>$v])->all();
      foreach($category as $n){
        $asd[$n->id] = $n->name;
      }
       $result[$value] = $asd;
    }

    $params = [
        'prompt' => 'Выберите категорию...',
    ];
    echo $form->field($model, 'category_id')->dropDownList($result,$params)->label('Категория');
    ?>

<?php echo $form->field($model, 'short_description')->widget(
    \yii\imperavi\Widget::class,
    [
        'plugins' => ['fullscreen', 'fontcolor', 'video'],
        'options' => [
            'minHeight' => 150,
            'maxHeight' => 150,
            'buttonSource' => true,
            'convertDivs' => false,
            'removeEmptyTags' => true,
            'placeholder' => 'Краткое описание. Будет показано над точкой на карте. Укажите только основную информацию',
            //'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
        ],
    ]
)->label('Краткое описание') ?>
    

    <?php echo $form->field($model, 'description')->widget(
    \yii\imperavi\Widget::class,
    [
        'plugins' => ['fullscreen', 'fontcolor', 'video'],
        'options' => [
            'minHeight' => 300,
            'maxHeight' => 300,
            'buttonSource' => true,
            'convertDivs' => false,
            'removeEmptyTags' => true,
            'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
            'placeholder' => 'Полное описание. Будет показано на отдельной странице. Оставьте максимально подробное описание(цены, время работы, ваши впечатления, что нравится, что не нравится) и не забудьте фото;)',
        ],
    ]
)->label('Полное описание') ?>


<?php echo $form->field($model, 'thumbnail')->widget(
    Upload::class,
    [
        'url' => ['../../backend/web/file/storage/upload'],
        'maxFileSize' => 5000000,
        'maxNumberOfFiles' => 10 // 5 MiB
    ])->label('Фотографии');
?>


<div class="form-group field-autocomplete1">
<label class="control-label" for="autocomplete1">Перетащите маркер на адрес объекта или введите точный адрев в поле "адрес обьекта" под картой</label>

 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>

 <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script> 
 <style type="text/css">
    #mapid { height: 45vh; }
   </style>

<div id="mapid"></div> 


</div>


<?php
       echo $form->field($model, 'address')->textInput(['id'=>'autocomplete', 'readonly' => false, 'placeholder'=>'Начинайте вводить адрес и получите подсказку'])->label('Точный адрес');
          echo $form->field($model, 'latitude')->textInput(['id'=>'latitude', 'readonly' => true,'placeholder'=>'Latitude'])->label(false);
          echo $form->field($model, 'longitude')->textInput(['id'=>'longitude', 'readonly' => true,'placeholder'=>'Longitude'])->label(false);
?>






    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
<?php
if($this->context->action->id == 'create'){
  if($model->latitude){$marker = $model->latitude.', '.$model->longitude;}else{$marker = '10.933211, 108.287184';}
}
if($this->context->action->id == 'update'){$marker = $model->latitude.', '.$model->longitude;}
?>


var mymap = L.map('mapid').setView([10.933211, 108.287184], 12);
//var mymap = L.map('mapid').setView([12.2450700, 109.1943200], 12);
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,    
    id: 'mapbox.streets'
  }).addTo(mymap);

 

  var marker = new L.marker([<?= $marker?>], {draggable: true}).addTo(mymap);
marker.on("dragend", function(e){
var lat = marker.getLatLng().lat.toFixed(8);
var lon = marker.getLatLng().lng.toFixed(8);
   $.get('https://nominatim.openstreetmap.org/reverse.php?format=json&lat='+lat+'&lon='+lon, function(data){
    document.getElementById('autocomplete').value = data.display_name;
    });
document.getElementById('latitude').value = lat;
document.getElementById('longitude').value = lon;


});

//center map
//map.panTo(new L.LatLng(40.737, -73.923));






</script>

<script>
             var autocomplete;
             function initAutocomplete() {
                // Create the autocomplete object, restricting the search to geographical
                // location types.
                autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                    {types: ['geocode']});

                // When the user selects an address from the dropdown, populate the address
                // fields in the form.
                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                  var place = autocomplete.getPlace();
                  $('#latitude').val(place.geometry.location.lat().toFixed(5));
                  $('#longitude').val(place.geometry.location.lng().toFixed(5));
                  mymap.removeLayer(marker);
                  marker = new L.marker([place.geometry.location.lat().toFixed(5), place.geometry.location.lng().toFixed(5)], {draggable: true}).addTo(mymap);
                  marker.on("dragend", function(e){
var lat = marker.getLatLng().lat.toFixed(8);
var lon = marker.getLatLng().lng.toFixed(8);
   $.get('https://nominatim.openstreetmap.org/reverse.php?format=json&lat='+lat+'&lon='+lon, function(data){
    document.getElementById('autocomplete').value = data.display_name;
    });
document.getElementById('latitude').value = lat;
document.getElementById('longitude').value = lon;


});

                      
              });
              } 

                   
</script>
<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDiern53s3oclBm52lQK0F-YWzLWCA_5BU&libraries=places&callback=initAutocomplete&language=en" type="text/javascript"></script>






