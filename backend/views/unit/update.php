<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Unit */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Unit',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'обьекта', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="unit-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
