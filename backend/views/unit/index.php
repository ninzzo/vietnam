<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Category;
use common\models\Unit;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обьекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Добавить обьект', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 

    $result = [];
    $items = Category::main_category();
    foreach($items as $v=>$value){

      $asd = [];
      $category = Category::find()->where(['category_id'=>$v])->all();
      foreach($category as $n){
        $asd[$n->id] = $n->name;
      }
       $result[$value] = $asd;
    }



    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'label'     => 'Название',
                'value' => function ($model) {
                    return $model->name;
                },
               
               
            ],

            [
                'attribute' => 'price',
                'label'     => 'Цена(VND) от',
                'value' => function ($model) {
                    return $model->price;
                },
               
               
            ],
         
            //'user_id',
            /*[
                'attribute' => 'image',
                'label'     => 'Превью',
                'format'    => 'html',
                'value' => function ($model) {
                    return Html::img($model->thumbnail_base_url.'/'.$model->thumbnail_path,
                    ['width' => '50px']);
                },
               
            ],*/
            [
                'attribute' => 'image',
                'label'     => 'Район',
                'value' => function ($model) {
                    return Unit::districtName($model->district);
                },
                'filter' => false,
               
            ],

            [
                'attribute' => 'category_id',
                'label'     => 'Категория',
                'value' => function ($model) {
                    $cat = Category::findOne($model->category_id);
                   return $cat->name;
                },
                'filter' => $result,
            ],
           

              

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{myButton}{update}{delete}',  // the default buttons + your custom button
                    'buttons' => [
                        'myButton' => function($url, $model, $key) {     // render your custom button
                            return Html::a( '<span class="glyphicon glyphicon-eye-open"></span>','/vietnam/frontend/web/unit/view?id='.$model->id );
                        }
                    ]
                ],
        ],
    ]); ?>

</div>
