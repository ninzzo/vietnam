<?php

use yii\db\Migration;

/**
 * Class m181024_154547_category_add_fields
 */
class m181024_154547_category_add_fields extends Migration
{
      public function up()
    {        
        $this->addColumn('{{%category}}', 'thumbnail_base_url', $this->string()->notNull());
        $this->addColumn('{{%category}}', 'thumbnail_path', $this->string()->notNull());
        $this->addColumn('{{%category}}', 'category_id', $this->string()->Null());
        
    }

    public function down()
    {
        $this->dropColumn('{{%category}}', 'thumbnail_base_url');
        $this->dropColumn('{{%category}}', 'thumbnail_path');
        $this->dropColumn('{{%category}}', 'category_id');
        
    }
}
