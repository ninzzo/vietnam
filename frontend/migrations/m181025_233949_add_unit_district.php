<?php

use yii\db\Migration;

/**
 * Class m181025_233949_add_unit_district
 */
class m181025_233949_add_unit_district extends Migration
{
     public function up()
    {        
        $this->addColumn('{{%unit}}', 'district', $this->integer()->notNull());
        
    }

    public function down()
    {
        $this->dropColumn('{{%unit}}', 'district');
       
    }
}
