<?php

use yii\db\Migration;

/**
 * Class m181025_160258_table_unit_image
 */
class m181025_160258_table_unit_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
          $this->createTable('{{%unit_image}}', [
            'id' => $this->primaryKey(),
            'unit_id' => $this->integer()->notNull(),
            'path' => $this->string()->notNull(),
            'base_url' => $this->string(),
            'type' => $this->string(),
            'size' => $this->integer(),
            'name' => $this->string(),
            'created_at' => $this->integer(),
            'order' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropTable('{{%unit_image}}');
    }

   
}
