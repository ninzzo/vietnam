<?php

use yii\db\Migration;

/**
 * Class m181023_144935_add_unit_coord_address
 */
class m181023_144935_add_unit_coord_address extends Migration
{
     public function up()
    {        
        $this->addColumn('{{%unit}}', 'latitude', $this->string()->Null());
        $this->addColumn('{{%unit}}', 'longitude', $this->string()->Null());
        $this->addColumn('{{%unit}}', 'address', $this->string()->Null());
    }

    public function down()
    {
        $this->dropColumn('{{%unit}}', 'latitude');
        $this->dropColumn('{{%unit}}', 'longitude');
        $this->dropColumn('{{%unit}}', 'address');
    }
}
