<?php

use yii\db\Migration;

/**
 * Handles the creation of table `unit`.
 */
class m181023_143900_create_unit_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%unit}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),   
            'price' => $this->decimal(10,2)->notNull(),
            'user_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'description' => $this->string()->notNull(),
            'image' => $this->string(), 
            
        ], $tableOptions);       
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('unit');
    }
}
