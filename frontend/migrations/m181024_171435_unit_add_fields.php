<?php

use yii\db\Migration;

/**
 * Class m181024_171435_unit_add_fields
 */
class m181024_171435_unit_add_fields extends Migration
{
      public function up()
    {        
        $this->addColumn('{{%unit}}', 'short_description', $this->string()->notNull());
        $this->addColumn('{{%unit}}', 'created_at', $this->string()->notNull());
        $this->addColumn('{{%unit}}', 'status', $this->integer()->notNull());
        
    }

    public function down()
    {
        $this->dropColumn('{{%unit}}', 'short_description');
        $this->dropColumn('{{%unit}}', 'created_at');
        $this->dropColumn('{{%unit}}', 'status');
        
    }

}
