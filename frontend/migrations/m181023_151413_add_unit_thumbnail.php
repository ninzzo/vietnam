<?php

use yii\db\Migration;

/**
 * Class m181023_151413_add_unit_thumbnail
 */
class m181023_151413_add_unit_thumbnail extends Migration
{
     public function up()
    {        
        $this->addColumn('{{%unit}}', 'thumbnail_base_url', $this->string()->notNull());
        $this->addColumn('{{%unit}}', 'thumbnail_path', $this->string()->notNull());
        
    }

    public function down()
    {
        $this->dropColumn('{{%unit}}', 'thumbnail_base_url');
        $this->dropColumn('{{%unit}}', 'thumbnail_path');
        
    }
}
