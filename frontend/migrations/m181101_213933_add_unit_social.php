<?php

use yii\db\Migration;

/**
 * Class m181101_213933_add_unit_social
 */
class m181101_213933_add_unit_social extends Migration
{
        public function up()
    {        
        $this->addColumn('{{%unit}}', 'fb', $this->string()->Null());
        $this->addColumn('{{%unit}}', 'vk', $this->string()->Null());
        $this->addColumn('{{%unit}}', 'telegram', $this->string()->Null());
        
    }

    public function down()
    {
        $this->dropColumn('{{%unit}}', 'fb');
        $this->dropColumn('{{%unit}}', 'vk');
        $this->dropColumn('{{%unit}}', 'telegram');
        
    }
}