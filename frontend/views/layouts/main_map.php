<?php
/* @var $this \yii\web\View */
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;

/* @var $content string */

$this->beginContent('@frontend/views/layouts/base.php')
?>
    <div class="container-fluid" style="padding-left:0px;padding-right:0px;margin-top:51px;">
    

        <?php echo $content ?>

    </div>
<?php $this->endContent() ?>