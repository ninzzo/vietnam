<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Category;
/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php')
?>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

  <script type="text/javascript">

   function changeFunc() {
    var selectBox = document.getElementById("district");
    var selectedValue = district.options[district.selectedIndex].value;
    document.location.href = 'http://localhost/vietnam/frontend/web/site/index?district='+selectedValue;
    //alert(selectedValue);
   }

  </script>
<style>




@media (max-width:1600px) { 
    .block-adres{
  position:fixed;
  width: 200px;
  height: 100px;
  left: 50px;
  top: 0;  
  color:white;
  margin-left:15px;
  margin-top: 60px;
  z-index: 1000;
} } 

@media (min-width:1600px) { 
    .block-adres{
  position:fixed;
  width: 200px;
  height: 100px;
  left: 5px;
  top: 36px;   
  color:white;
  margin-left:15px;
  margin-top: 10px;
  z-index: 1000;
} } 

@media (max-width:767px) { 
    .block-adres{
  position:fixed;
  width: 200px;
  height: 100px;
  left: 5px;
  top: 36px;  
  color:white;
  margin-left:0px;
  margin-top: 0px;
 

} 


} 



.main_cat_label{
    font-size:18px;
    padding-top: 10px;
    

}
.sub_cat_label{    
    margin-left: 25px;    
}

</style>


<div class="wrap">

    <?php
    if($this->context->action->id == 'index' AND Yii::$app->controller->id == 'site') {
        $units = Category::find()->all();
    ?>
    <div class="block-adres"> 
    
    
    <p style="margin-top: 15px;">
        <button class="btn btn-info" style="width:200px" data-toggle="collapse" data-target=".qwe">
        <span id="test" class="collapse in qwe">Скрыть фильтр</span>
        <span id="test" class="collapse qwe">Показать фильтр</span>
      </button>
    </p>
    <div id="hide-me" class="collapse in qwe" style="padding-bottom: 10px;margin-top:-10px;background: white;color: black;padding-left: 15px;opacity: 0.9;">
    
      <div class="form-inline" style="width:170px;padding-right: 10px;padding-top: 5px"> 
                <select style="width:100%" id="district" class="form-control" onchange="changeFunc();">
                    <?php 
                    if(isset($_SESSION['district'])){}else{$_SESSION['district'] = 1;}
                    ?>
                  <option value="1" <?php if($_SESSION['district'] == 1){echo 'selected';}?> ><?=Yii::t('frontend', 'Mui Ne')?></option>
                  <option value="2" <?php if($_SESSION['district'] == 2){echo 'selected';}?> ><?=Yii::t('frontend', 'Nha Trang')?></option>
                </select>    
    </div>
    <div class="checkbox">
    <label class="main_cat_label">
      <input type="checkbox" class="main_cat" value="1" > Еда
    </label>
    <?php
    foreach($units as $u){
        if($u['category_id']==1){echo '<div><label class="sub_cat_label cat_1">
      <input type="checkbox" checked class="sub_cat" value="'.$u['id'].'" onclick="del()"> '.$u['name'].'
    </label></div>';}
    }
    ?>
    
    <label class="main_cat_label">
      <input type="checkbox"  class="main_cat" name="nameOfChoice" value="2" > Отдых
    </label>
    <?php
    foreach($units as $u){
        if($u['category_id']==2){echo '<div><label class="sub_cat_label cat_2">
      <input type="checkbox" checked class="sub_cat" value="'.$u['id'].'" onclick="del()"> '.$u['name'].'
    </label></div>';}
    }
    ?>
    
    <label class="main_cat_label">
      <input type="checkbox"  class="main_cat" name="nameOfChoice" value="3" > Магазины
    </label>
    <?php
    foreach($units as $u){
        if($u['category_id']==3){echo '<div><label class="sub_cat_label cat_3">
      <input type="checkbox" checked class="sub_cat" value="'.$u['id'].'" onclick="del()"> '.$u['name'].'
    </label></div>';}
    }
    ?>
    
    <label class="main_cat_label">
      <input type="checkbox"  class="main_cat" name="nameOfChoice" value="4" > Услуги
    </label>
    <?php
    foreach($units as $u){
        if($u['category_id']==4){echo '<div><label class="sub_cat_label cat_4">
      <input type="checkbox" checked class="sub_cat" value="'.$u['id'].'" onclick="del()"> '.$u['name'].'
    </label></div>';}
    }
    ?>
    
    <label class="main_cat_label">
      <input type="checkbox"  class="main_cat" name="nameOfChoice" value="5" > Аренда
    </label>
    <?php
    foreach($units as $u){
        if($u['category_id']==5){echo '<div><label class="sub_cat_label cat_5">
      <input type="checkbox" checked class="sub_cat" value="'.$u['id'].'" onclick="del()"> '.$u['name'].'
    </label></div>';}
    }
    ?>
    
    <label class="main_cat_label">
      <input type="checkbox"  class="main_cat" name="nameOfChoice" value="6" > Спорт
    </label>
    <?php
    foreach($units as $u){
        if($u['category_id']==6){echo '<div><label class="sub_cat_label cat_6">
      <input type="checkbox" checked class="sub_cat" value="'.$u['id'].'" onclick="del()"> '.$u['name'].'
    </label></div>';}
    }
    ?>

    <label class="main_cat_label">
      <input type="checkbox"  checked class="main_cat" name="nameOfChoice" value="7" > Все точки
    </label>
    
    </div>
    </div>
    
    </div>
    <?php
    }
    ?>



    <?php
    NavBar::begin([
        'brandLabel' => 'Vietnam Map',
        'brandUrl' => Url::to(['/site/index']),
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]); ?>
    <?php echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
      
            //['label' => Yii::t('frontend', 'Home'), 'url' => ['/site/index']],
            //['label' => Yii::t('frontend', 'About'), 'url' => ['/page/view', 'slug'=>'about']],
            //['label' => Yii::t('frontend', 'Articles'), 'url' => ['/article/index']],
            //['label' => Yii::t('frontend', 'Contact'), 'url' => ['/site/contact']],
            ['label' => Yii::t('frontend', 'Add object'), 'url' => ['/unit/create'], 'visible'=>Yii::$app->user->isGuest],
            ['label' => Yii::t('frontend', 'Signup'), 'url' => ['/user/sign-in/signup'], 'visible'=>Yii::$app->user->isGuest],
            ['label' => Yii::t('frontend', 'Login'), 'url' => ['/user/sign-in/login'], 'visible'=>Yii::$app->user->isGuest],
            [
                'label' => Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->getPublicIdentity(),
                'visible'=>!Yii::$app->user->isGuest,
                'items'=>[
                    [
                        'label' => Yii::t('frontend', 'Add object'),
                        'url' => ['/unit/create']
                    ],
                    [
                        'label' => Yii::t('frontend', 'My objects'),
                        'url' => ['/unit/index']
                    ],

                    [
                        'label' => Yii::t('frontend', 'Settings'),
                        'url' => ['/user/default/index']
                    ],
                    /*[
                        'label' => Yii::t('frontend', 'Backend'),
                        'url' => Yii::getAlias('@backendUrl'),
                        'visible'=>Yii::$app->user->can('manager')
                    ],*/
                    [
                        'label' => Yii::t('frontend', 'Logout'),
                        'url' => ['/user/sign-in/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ]
                ]
            ],
            [
                'label'=>Yii::t('frontend', 'Language'),
                'items'=>array_map(function ($code) {
                    return [
                        'label' => Yii::$app->params['availableLocales'][$code],
                        'url' => ['/site/set-locale', 'locale'=>$code],
                        'active' => Yii::$app->language === $code
                    ];
                }, array_keys(Yii::$app->params['availableLocales']))
            ]
        ]
    ]); ?>
    <?php NavBar::end(); ?>

    <?php echo $content ?>

</div>


<footer class="footer">
    <div class="container">
        <!--<p class="pull-left">&copy; My Company <?php echo date('Y') ?></p>-->
        <p class="pull-right"><?=Yii::t('frontend', 'Do you have any ideas or comments?') ?> <a href="https://web.telegram.org/#/im?p=g274230986"><?=Yii::t('frontend', 'Write me')?></a></p>
    </div>
</footer>
<?php $this->endContent() ?>