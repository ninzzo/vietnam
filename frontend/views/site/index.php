<?php
/* @var $this yii\web\View */
$this->title = Yii::$app->name;
?>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>
<!-- Make sure you put this AFTER Leaflet's CSS -->
 <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
   
   <style type="text/css">
   	#mapid { height: 100vh; }

   </style>



<div class="site-index">
  




    <div class="body-content">

        <div class="row">
        	
          
            <div class="col-lg-12">
                <div id="mapid"></div>
                

         

                


            </div>
        </div>

    </div>
</div>

<?php


$data = [];
foreach($model as $v){
$item = [$v->name, $v->latitude, $v->longitude, $v->category->thumbnail_base_url.'/'.$v->category->thumbnail_path, $v->category_id, $v->category->category_id, $v->short_description, $v->id, date("d.m.y",$v->created_at)]; 
array_push($data, $item);
}
/*print_r($data);
die();*/
$json = json_encode($data); 
?>
<style>
    .leaflet-verticalcenter {
      
      position: absolute;
      z-index: 1000;
      pointer-events: none;      
      top: 50%;
      transform: translateY(-50%);
      padding-top: 10px;
    }
    
    .leaflet-verticalcenter .leaflet-control {
      margin-bottom: 10px;
    }
  </style>
<script>

var markers = [];

var planes = <?php echo $json; ?>;
//console.log(planes);

<?php if(isset($_SESSION['district'])){}else{$_SESSION['district'] = 1;}
if($_SESSION['district'] == 1){echo 'var mymap = L.map("mapid").setView([10.933211, 108.287184], 12);';}
if($_SESSION['district'] == 2){echo 'var mymap = L.map("mapid").setView([12.2450700, 109.1943200], 12);';}
?>

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,		
		id: 'mapbox.streets'
	}).addTo(mymap);


// Create additional Control placeholders
    function addControlPlaceholders(map) {
      var corners = map._controlCorners,
        l = 'leaflet-',
        container = map._controlContainer;

      function createCorner(vSide, hSide) {
        var className = l + vSide + ' ' + l + hSide;

        corners[vSide + hSide] = L.DomUtil.create('div', className, container);
      }

      createCorner('verticalcenter', 'left');
      createCorner('verticalcenter', 'right');
    }
    addControlPlaceholders(mymap);

    // Change the position of the Zoom Control to a newly created placeholder.
    mymap.zoomControl.setPosition('verticalcenterright');



function add_markers()
{

  for (var i = 0; i < planes.length; i++) {
    
    var customPopup = '<a href="/vietnam/frontend/web/unit/view?id='+planes[i][7]+'"><h3>' + planes[i][0] + '</h3></a><hr>'
           + '<div>' + planes[i][6] + '</div><hr><i class="glyphicon glyphicon-calendar"></i> '+planes[i][8]+'<a class="pull-right" href="/vietnam/frontend/web/unit/view?id='+planes[i][7]+'">подробнее</a>';
    var customOptions =
        {
        'maxWidth': '300',
        'minWidth': '300',
        

        
        };

      var myicon = L.icon({
        iconUrl: planes[i][3],
        iconSize:     [32, 32],
      });

      marker = new L.marker([planes[i][1],planes[i][2]], {icon: myicon})
        .addTo(mymap)  
        .bindPopup(customPopup,customOptions)
        .on('popupopen', function (popup) {
        $('.block-adres').hide();
    }).on('popupclose', function (popup) {
        $('.block-adres').show();
    });;  
        


    var id;
      if (markers.length < 1) id = 0
      else id = markers[markers.length - 1]._id + 1;
    marker._id = id;
    marker.category_id = planes[i][4];
    marker.main_category = planes[i][5];
    markers.push(marker);
    }
}
add_markers();

	


function del() {




var checked = [];
$(".main_cat:checked").each(function() {
    checked.push(+$(this).val());
});

console.log(checked);

var sub_checked = [];
$('.sub_cat:checked').each(function() {
    sub_checked.push(+$(this).val());
});

console.log(sub_checked);


if(checked.indexOf(1) != -1){ $('.cat_1').show(); } else { $('.cat_1').hide();}
if(checked.indexOf(2) != -1){ $('.cat_2').show(); } else { $('.cat_2').hide();}
if(checked.indexOf(3) != -1){ $('.cat_3').show(); } else { $('.cat_3').hide();}
if(checked.indexOf(4) != -1){ $('.cat_4').show(); } else { $('.cat_4').hide();}
if(checked.indexOf(5) != -1){ $('.cat_5').show(); } else { $('.cat_5').hide();}
if(checked.indexOf(6) != -1){ $('.cat_6').show(); } else { $('.cat_6').hide();}
if(checked.indexOf(7) != -1){ $('.cat_7').show(); } else { $('.cat_7').hide();}

  markers.forEach(function(marker) {
    if (checked.indexOf(+marker.main_category) == -1 || sub_checked.indexOf(+marker.category_id) == -1){ mymap.removeLayer(marker); } else { mymap.addLayer(marker); }
    //all
 if(checked.indexOf(7) != -1){ mymap.addLayer(marker); }
    
  });


}

$('.main_cat').on("change", function(e){ 

    $('.main_cat').each(function()
    { 
      if(e.target != this)
        this.checked = false; 
    });
    del();
});

del();
</script>