<?php

use yii\widgets\Breadcrumbs;
use branchonline\lightbox\Lightbox;
use common\models\UnitImage;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\Unit */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Objects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>
<!-- Make sure you put this AFTER Leaflet's CSS -->
 <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
   
   <style type="text/css">
    #mapid { height: 100vh; }
    .cont{margin-top: 20px;}
    .descr{margin-top: 15px;}
    .photo{margin-top: 15px;}
    .photo img{width:200px;height:100px;}

@media (max-width:1199px) { 
 #mapid { height: 50vh; }
 .cont{margin:15px;}

  } 


   </style>


<div class="site-index">
  




    <div class="body-content">

        <div class="row">
            
          
            <div class="col-lg-6">
               
                <div id="mapid"></div>

         

                


            </div>

            <div class="col-lg-6">
              
                <div class="cont">

                
                    <?php echo Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>

                    <div class="desc">
                
                        <?php 
              if(Yii::$app->user->id == $model->user_id){
                echo Html::a('Редактировать', ['update', 'id'=>$model->id], ['class' => 'btn btn-primary btn-sm', ]);

              }
              ?>

                    </div>
                    
                    <h1><?=$model->name?></h1>
                    <div class="descr"><?=$model->description?></div>
                   

                    <div class="social">
<?php
if($model->fb){echo '<a target="_blank" href="'.$model->fb.'"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPUjO_ziPV52a94Lev5wZGfdbKM1rAo8MzUZAZ2qbCPDY_ByIiEw" width="30px"></a>';}
?>

<?php
if($model->vk){echo ' <a target="_blank" href="'.$model->vk.'"><img src="http://www.paintnumber.by/wp-content/uploads/bfi_thumb/vk-icon-30xkoj9q73f9f4ptfpzfuy.png" width="30px"></a>';}
?>

<?php
if($model->telegram){echo ' <a target="_blank" href="'.$model->telegram.'"><img src="http://www.stickpng.com/assets/images/5842a8fba6515b1e0ad75b03.png" width="30px"></a>';}
?>
                    </div>

                     <div class="descr"><i class="glyphicon glyphicon-calendar"></i> <?=date("d.m.y",$model->created_at)?></div>
                    

<div class="photo">
<?php
$images = UnitImage::find()->where(['unit_id'=>$model->id])->all();
if($images){}
foreach($images as $i){

echo Lightbox::widget([
    'files' => [
        [
            'thumb' => $i['base_url'].'/'.$i['path'],
            'original' => $i['base_url'].'/'.$i['path'],
            'title' => $model->name,
        ],
    ]
]);

}
 
?>
</div>

                </div>






                </div>

            </div>
        </div>

    </div>
</div>


<script>

var mymap = L.map("mapid").setView([<?=$model->latitude?>,<?=$model->longitude?>], 12);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,        
        id: 'mapbox.streets'
    }).addTo(mymap);

    var myicon = L.icon({
        iconUrl: '<?=$model->category->thumbnail_base_url.'/'.$model->category->thumbnail_path?>',
        iconSize:     [32, 32],
      });

      marker = new L.marker([<?=$model->latitude?>,<?=$model->longitude?>], {icon: myicon})
        .addTo(mymap)  

</script>