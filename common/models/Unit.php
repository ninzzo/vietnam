<?php

namespace common\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use common\models\Category;
/**
 * This is the model class for table "unit".
 *
 * @property int $id
 * @property string $name
 * @property string $price
 * @property int $user_id
 * @property int $category_id
 * @property string $description
 * @property string $image
 */
class Unit extends \yii\db\ActiveRecord
{   

    public $thumbnail;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'category_id', 'description', 'short_description'], 'required'],
            [['price'], 'number'],
            [['user_id', 'category_id', 'district'], 'integer'],
            [['name', 'description', 'image'], 'string'],
            [['latitude', 'longitude', 'address'], 'string', 'max' => 255],
            //[['thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['attachments', 'thumbnail', 'created_at', 'status', 'category_id', 'fb', 'vk', 'telegram'], 'safe'],
        ];
    }

     public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public static function districtName($id)
    {
        if($id == 1){return 'Муйне';}
        if($id == 2){return 'Нячанг';}
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->user_id = \Yii::$app->user->id;
            $this->status = 1;
            $this->created_at = time();
            $this->thumbnail_base_url = '';
            $this->thumbnail_path = '';
            return true;
        } else {
            return false;
        }
    } 

     public function behaviors()
    {
        return [
            
             [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'multiple' => true,
                'uploadRelation' => 'images',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
                'orderAttribute' => 'order',
                'typeAttribute' => 'type',
                'sizeAttribute' => 'size',
                'nameAttribute' => 'name',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'user_id' => Yii::t('app', 'User ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'description' => Yii::t('app', 'Description'),
            'image' => Yii::t('app', 'Image'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'address' => Yii::t('app', 'Address'),


        ];
    }


        /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(UnitImage::class, ['unit_id' => 'id']);
    }
}
