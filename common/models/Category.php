<?php

namespace common\models;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property string $description
 * @property string $image
 */
class Category extends \yii\db\ActiveRecord
{   
    public $thumbnail;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','category_id'], 'required'],
            [['user_id'], 'integer'],
            [['name', 'description', 'image'], 'string', 'max' => 255],
            [['thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['attachments', 'thumbnail', 'category_id', 'user_id', 'description'], 'safe'],
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->user_id = \Yii::$app->user->id;
            $this->description = '';
            return true;
        } else {
            return false;
        }
    } 


       public function behaviors()
    {
        return [
            
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'user_id' => Yii::t('app', 'User ID'),
            'description' => Yii::t('app', 'Description'),
            'image' => Yii::t('app', 'Image'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }

    public static function main_category(){
        return [
                        '1'=>'Еда',
                        '2'=>'Отдых',
                        '3'=>'Магазины',
                        '4'=>'Услуги',
                        '5'=>'Аренда',
                        '6'=>'Спорт',
                    ];
    }
}
